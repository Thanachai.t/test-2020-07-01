import '../styles/globals.css'
import React from 'react'
// import { wrapper, store } from "../store/index.ts"
import { Provider } from "react-redux"
import rootReducer from '../stores/index.ts'
import { createStore, applyMiddleware } from 'redux'
import logger from 'redux-logger'

const store = createStore(rootReducer, applyMiddleware(logger));

function MyApp({ Component, pageProps }) {
  return (
    <Provider store={store}>
      <Component {...pageProps} />
    </Provider>
  )
}

export default MyApp
