import axios from 'axios'
import React from 'react'
import { Component } from 'react'

class Foo extends Component {
  constructor(props) {
    super(props);

    this.state = { search: '', users: [{ foo: 'bar' }] };
  }
  async search() {
    let r = await axios.get(`/api/users/get`, { params: { search: this.state.search } })

    this.setState({ users: r.data })
  }

  handleChange(event) {
    this.setState({ search: event.target.value });
  }

  async add() {
    let r = await axios.post('/api/users/add')
  }

  async remove(userId) {
    let r = await axios.post('/api/users/remove', { params: { userId } })
  }

  render() {
    const rows = []
    for (const index in this.state.users) {
      const user = this.state.users[index]
      rows.push(<div>
        <div>{index}</div>
        <div>{user.first_name}</div>
        <div>{user.last_name}</div>
        <div>
          <button type="button" onClick={() => this.remove(user.id)}>ลบ</button>
        </div>
      </div>)
    }
    return (<>
      <div>
        <div>
          <input type="text" value={this.state.search} onChange={(evt) => this.handleChange(evt)} />
          <button type="button" onClick={() => this.search()}>ค้นหา</button>
        </div>
        <div>
          <div>
            <div>
              Username:
              <input type="text" />
            </div>
            <div>
              first_name:
              <input type="text" />
            </div>
            <div>
              last_name:
              <input type="text" />
            </div>
            <div>
              email:
              <input type="text" />
            </div>
            <div>
              Address:
              <input type="text" />
            </div>
            <div>
              Province:
              <input type="text" />
            </div>
            <div>
              khet:
              <input type="text" />
            </div>
          </div>
          <div>
            <button type="button" onClick={() => this.add()}>เพิ่ม</button>
          </div>
        </div>
        <div>
          {rows}
        </div>
        <pre>{JSON.stringify(this.state.users, null, 4)}</pre>
      </div>
    </>);
  }
}

// export const getInitialProps = async ctx => {
//   try {
//     const res = await axios.get(`http://localhost:3000/api/hello`);
//     const restaurants = res.data;
//     console.log(restaurants);
//     return { restaurants };
//   } catch (error) {
//     return { error };
//   }
// }

export default Foo;
