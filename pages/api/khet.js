// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
export default async function handler(req, res) {
  const p = req.query.province
  const khets = {
    p1: ['k1.1', 'k1.2'],
    p2: ['k2.1', 'k2.2']
  }
  res.status(200).json(khets[p])
}