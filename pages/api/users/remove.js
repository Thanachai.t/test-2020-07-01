// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import { Request } from 'tedious'
import dbConnection from '../../../utils/mssql'

export default async function handler(req, res) {
  const userId = req.params.id

  conn.query(
    'DELETE user WHERE id = ?',
    [userId], function (err, results, fields) {
      res.status(200).json({ success: true })
    })
}