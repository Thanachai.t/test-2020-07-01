// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import { Request } from 'tedious'
import { getObjList } from '../../../utils/mssql'
import conn from '../../../utils/mysql'

export default async function handler(req, res) {
  const search = req.query.search

  conn.query('SELECT * FROM user WHERE first_name LIKE ? OR last_name LIKE ?', [`%${search}%`, `%${search}%`], function (err, results, fields) {
    res.status(200).json(results)
  })
}
