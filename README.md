# Run project
edit config at [/config.js](config.js)
```bash
yarn install
yarn dev

```

# ER Diagram
```mermaid
erDiagram
    Customer ||--o{ Product : order
    Customer {
        int customer_id PK
        string customer_name
        string customer_address
        string customer_birthdate
        string customer_type
    }
    
    Product {
        int customer_id PK
        string ProductName
        int Price
    }
    
```

# Sequence Diagram
```mermaid
sequenceDiagram
    Customer->>System: send infomation(name, birdate, address)
    System-->>Customer: saved
    Customer-)System: send order
    System-->>Customer: saved
```

# SQL

## query Customer sort by price
```SQL
SELECT	
	c.customer_id,
	count(p.Price) AS price_amont
FROM Customer c LEFT JOIN Product p ON c.customer_id = p.customer_id
GROUP BY c.customer_id
ORDER BY price_amont DESC

```

## Customer type

```SQL
UPDATE Customer 
SET
	customer_type = IIF(p.price_amont >= 100000, 'Premium', IIF(p.price_amont > 35000, 'Gold', 'Silver'))
FROM (
	SELECT 
		customer_id,
		COUNT(Price) as price_amont 
	FROM
		Product 
	GROUP BY customer_id
) as p
INNER JOIN Customer c ON p.customer_id = c.customer_id
WHERE c.customer_id = p.customer_id
```

## get Customer ที่เกิดวันนี้
```SQL
SELECT
	customer_id,
	customer_name,
	customer_type
FROM 
	Customer
WHERE
 customer_birthdate	= CAST( GETDATE() AS Date )

```