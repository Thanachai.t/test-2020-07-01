import { Connection } from 'tedious'
import { Request } from 'tedious'

class PromisedConnection {
  constructor(config) {
    // this.connection always points to a promise resolving after the last assigned request
    // initial setting is either resolved once a new connection is established or rejected if an error occurs
    this.connection = new Promise((resolve, reject) => {
      const dbConnection = new Connection(config);
      dbConnection.on("connect", function (err) {
        if (err) {
          console.warn(err);
          reject(err);
        }
        else {
          resolve(dbConnection);
        }
      });
    });
  }

  execute(request) {
    const nextConnection = new Promise((resolve, reject) => {
      // after scheduling new request this.connection should be reassigned to be the last in promise queue
      this.connection
        .catch((reason) => {
          reject(reason);
        })
        .then((dbConnection) => { // a new request can be executed only within the connection is free again (resolved after the last request)
          request.on("requestCompleted", () => { // add an additional event listener in order to release connection after the request is done
            resolve(dbConnection);
          });
          dbConnection.execSql(request);
        });
    });
    this.connection = nextConnection;
  }
}
const dbConnection = new PromisedConnection({
  server: 'localhost',
  authentication: {
    type: 'default',
    options: {
      userName: 'sa',
      password: 'abcABC123'
    }
  },
  options: {
    database: 'simple'
  }
})


function getObjList(query, parameters = []) {
  return new Promise((resolve, reject) => {
    const objList = [];
    let request = new Request(
      query,
      function (err, rowCount, rows) {
        if (err) {
          reject(err);
        } else if (rowCount < 1) {
          reject(new Error("0 rows returned from DB"));
        }
      }
    );
    for (const { name, type, value } of parameters) {
      request.addParameter(name, type, value);
    };

    request.on("row", (columns) => {
      objList.push(Obj.fromColumns(columns)); // here I just make a specific object from each row
    });

    request.on("requestCompleted", () => {
      resolve(objList);
    });

    dbConnection.execute(request);
  });
}

export {
  dbConnection,
  getObjList
}