import mysql from 'mysql2'
import conf from "../config"

var connection = mysql.createConnection(conf.mysql);

export default connection
