export const reducer = (state = 0, action) => {
  switch (action.type) {
    case 'INCREMENT':
      return state + action.score;
    case 'DECREMENT':
      return state - action.score;
    default:
      return state;
  }
};

export const action = {
  increment: (score = 1) => ({
    type: 'INCREMENT',
    score
  }),
  decrement: (score = -1) => ({
    type: 'DECREMENT',
    score
  })
}
