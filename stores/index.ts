import { combineReducers } from 'redux';
import { reducer } from './counters.ts';

export default combineReducers({
  counters: reducer
});
